package com.gt.dev.lazaro.requesttowordpressapirest;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.gt.dev.lazaro.requesttowordpressapirest.adapter.PostAdapter;
import com.gt.dev.lazaro.requesttowordpressapirest.adapter.PostGS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<PostGS> posts = new ArrayList<>();
    protected static String url = "http://dev.lazarusgt.com/wp-json/wp/v2/posts";
    private RecyclerView rvMain;
    private ProgressDialog progressDialog;
    private RequestQueue req;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvMain = findViewById(R.id.rv_main);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        rvMain.setLayoutManager(manager);
        rvMain.setHasFixedSize(true);
        rvMain.setItemAnimator(new DefaultItemAnimator());

        req = Volley.newRequestQueue(this);
        loadDialog();
        getPosts();
    }

    private void setUpAdapter(ArrayList<PostGS> posts) {
        this.rvMain.setAdapter(new PostAdapter(posts));
    }

    private void loadDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading posts...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    private void getPosts() {
        JsonArrayRequest request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("POST", response.toString());

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject post = (JSONObject) response.get(i);
                        JSONObject postTitle = post.getJSONObject("title");

                        String title = postTitle.getString("rendered");
                        posts.add(new PostGS(title));

                        setUpAdapter(posts);
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("VOLLEY-ERROR", error.getMessage());
            }
        });
        req.add(request);
    }

}
