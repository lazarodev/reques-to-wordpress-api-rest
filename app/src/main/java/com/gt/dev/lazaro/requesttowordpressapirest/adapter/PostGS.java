package com.gt.dev.lazaro.requesttowordpressapirest.adapter;

/**
 * Created by LazaroDev on 14/12/2017.
 */

public class PostGS {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PostGS(String title) {
        this.title = title;
    }

}
