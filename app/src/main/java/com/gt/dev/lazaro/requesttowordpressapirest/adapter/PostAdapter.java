package com.gt.dev.lazaro.requesttowordpressapirest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gt.dev.lazaro.requesttowordpressapirest.R;

import java.util.List;

/**
 * Created by ILIFEBELT on 14/12/2017.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    private List<PostGS> postList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle;

        public MyViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_post_title);
        }

    }

    public PostAdapter(List<PostGS> postList) {
        this.postList = postList;
    }

    @Override
    public PostAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostAdapter.MyViewHolder holder, int position) {
        PostGS postGs = postList.get(position);
        holder.tvTitle.setText(postGs.getTitle());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
